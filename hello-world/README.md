# Primeiro Deploy - Mailson Dennis

Este documento descreve os passos seguidos para realizar o primeiro deploy na AWS.

## Conectando a VM

Primeiro de tudo precisamo no conectar a VM na AWS para que possamos fazer o deploy. Para isso vamos definir a variável:

```
MASTER=ec2-44-227-104-43.us-west-2.compute.amazonaws.com
```

Vamos precisar também da key `*.pem`. Nesse exemplo, a nossa key tem o nome de `ifpb-alunos-key.pem`, por tanto usaremos o seguinte comando para se conectar ao terminal da VM:

```
ssh -i "ifpb-alunos-key.pem" ubuntu@$MASTER
```

Com a conexão estabelecida podemos fazer o clone deste repositório dentro da VM e então fazer o deploy do nosso container docker, que terá o passo a passo explicado no próximo tópico

## Fazendo o deploy do container

Primeiro de tudo, precisamos gerar um `.jar` do nosso código para que seja possível a criação do container.

Vamos fazer isso utilziando o `maven` com o seguinte comando:

```
mvn clean package
```

Após a conclusão do comando, podemos gerar a imagem para o nosso container com o comando:

Obs: é necessário estar dentro da pasta do projeto
```
sudo docker build -t mailson/hello-world .
``` 

Antes de gerar o nosso container, vamos definir uma variável para a porta que será usada na aplicação. Escolhemos a porta 8085.

```
WSPORT=8085
```

Agora podemos criar o nosso container e passar a porta como parâmetro para o container com o seguinte comando:

```
sudo docker run -e WSPORT=$WSPORT -p $WSPORT:$WSPORT --name hello_mailson mailson/hello-world
```

Com isso, nosso container estará rodando na VM. E se abrirmos outro terminal na VM podemos relaizar uma requisição para `api/status` e iremos receber um ok de retorno. Podemos visualizar isso com o comando:

Obs: A variável `$WSPORT` deve estar definida neste terminal também

```
curl localhost:$WSPORT/api/status
```


