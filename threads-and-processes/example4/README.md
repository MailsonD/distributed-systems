# Utilizando um Buffer circular sincronizado com a classe ArrayBlockingQueue

No exemplo anterior tratamos do problema de sincronização e da ociosidade das Threads quanto ao recurso do Buffer. Entretanto, a solução utilizada começou a deixar nosso código maior e mais verboso. Para utilizarmos do mesmo modelo de solução, mas diminuindo o nosso código, podemos utilziar da classe ArrayBlockingQueue, que nada mais é do que é um buffer circular e sincronizado já implementado em java.
