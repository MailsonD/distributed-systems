package flow;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PoemBufferBlocking implements Buffer {

    private final ArrayBlockingQueue<String> buffer;

    public PoemBufferBlocking() {
        this.buffer = new ArrayBlockingQueue<>(3);
    }

    @Override
    public void set(String value) {
        try {
            buffer.put(value);
            System.out.printf("Producing value: %s", value);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String get() {
        String consumedValue = "";
        try {
            consumedValue = buffer.take();
            System.out.printf("Consuming value: %s", consumedValue);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return consumedValue;
    }
}
