package flow;

public interface Buffer {

    void set(String value);
    String get();

}
