import flow.Buffer;
import flow.Consumer;
import flow.PoemBuffer;
import flow.Producer;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class App {

    public static void main(String[] args) {
        ExecutorService service = Executors.newFixedThreadPool(2);

        Buffer sharedBuffer = new PoemBuffer();

        try {
            service.execute(new Producer(sharedBuffer));
            service.execute(new Consumer(sharedBuffer));

        }catch (Exception e) {
            e.printStackTrace();
        }
        service.shutdown();
    }


}
