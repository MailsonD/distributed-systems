package flow;

public class PoemBuffer implements Buffer{

    private String buffer;

    public PoemBuffer() {
        this.buffer = "";
    }

    @Override
    public void set(String value) {
        System.out.printf("Producing value: %s", value);
        buffer = value;
    }

    @Override
    public String get() {
        System.out.printf("Consuming value: %s", buffer);
        return buffer;
    }
}
