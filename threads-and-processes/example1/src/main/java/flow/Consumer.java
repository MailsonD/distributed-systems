package flow;

import java.util.List;
import java.util.Random;

public class Consumer implements Runnable {

    private final static Random random = new Random();
    private final Buffer sharedBuffer;

    public Consumer(Buffer sharedBuffer) {
        this.sharedBuffer = sharedBuffer;
    }

    public void run() {
        StringBuilder poem = new StringBuilder();
        for(int count = 1; count <=5; count++) {
            try {
                Thread.sleep(random.nextInt(3000));
                poem.append(sharedBuffer.get());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        System.out.println("Finished consumption!!!");
        System.out.printf("End value: \n%s", poem.toString());
    }

}
