# Utilizando multithread no modelo produtor/consumidor

Neste exemplo construímos uma aplicação no modelo produtor/consumidor utilizar do auxilio de threads. Esta abordagem, apesar de ser bastante interessante em diversos cenários, entretanto, traz consigo uma enorme quantidade de problemas que virão ser resolvidos nos exemplos seguintes.

## Utilizando um Buffer

Para que possamos implementar o modelo produtor/consumidor precisamos primeiro de um recurso que possa ser compartilhado tanto pela Thread de produção qunanto pela de Consumo, pois dessa maneira as Threads poderão manter algum tipo de comunicação.

Nesse exemplo o Buffer foi feito como uma interface com um método <i>set</i> e um método <i>get</i> para manipular o valor que se encontra dentro do buffer.

O Buffer utilizado nessa aplicação é a implementação da interface Buffer, chamado de PoemBuffer. Nele armazenamos o valor que será produzido em uma única variável, e o lemos através do consumo.

## Produtor

O produtor retem um Array com os versos do poema que será enviado, parte a parte, para o buffer, e faz isso em intervalos variáveis para gerar uma certa aleatoriedade entre a Thread de produção e a de consumo. O produtor possui também uma referência de um objeto Buffer que será usado também pelo consumidor.

## Consumidor

O consumidor por sua vez apenas lê o ultimo valor adicionado ao Buffer, caso exista algum, e o imprime na tela. O resultado final deveria ser o poema com os versos em ordem. Porém, veremos que esse exmplo tráz alguns problemas.
