package flow;

import java.util.List;
import java.util.Random;

public class Producer implements Runnable {

    private final static Random random = new Random();
    private final Buffer sharedBuffer;
    //This is a poem called Like The Moon
    private final List<String> poem = List.of(
            "I will be\n",
            "like the moon\n",
            "and learn to shine\n",
            "even when\n",
            "I am not whole.\n"
    );

    public Producer(Buffer sharedBuffer) {
        this.sharedBuffer = sharedBuffer;
    }

    public void run() {
        poem.forEach(phrase -> {
            try {
                Thread.sleep(random.nextInt(3000));
                sharedBuffer.set(phrase);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        System.out.println("Finished production!!!");
    }
}
