package flow;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PoemBuffer implements Buffer {

    private final Lock accessLock = new ReentrantLock();
    private final Condition canWrite = accessLock.newCondition();
    private final Condition canRead = accessLock.newCondition();

    private final String[] buffer;
    private Integer quantityInBuffer;
    private Integer writeIndex;
    private Integer readIndex;

    public PoemBuffer() {
        this.buffer = new String[]{"", "", ""};
        this.quantityInBuffer = 0;
        this.writeIndex = 0;
        this.readIndex = 0;
    }

    @Override
    public void set(String value) {
        accessLock.lock();
        try {
            while(quantityInBuffer == buffer.length) {
                System.out.println("Trying to write");
                System.out.println("The buffer is full. The producer awaits!");
                canWrite.await();
            }
            buffer[writeIndex] = value;
            writeIndex = (writeIndex + 1) % buffer.length;
            quantityInBuffer++;
            System.out.printf("Producing value: %s", value);
            canRead.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }

    }

    @Override
    public String get() {
        String consumedValue = "";
        accessLock.lock();
        try {
            while(quantityInBuffer == 0) {
                System.out.println("Trying to read");
                System.out.println("Buffer empty. The consumer awaits!");
                canRead.await();
            }
            consumedValue = buffer[readIndex];
            readIndex = (readIndex + 1) % buffer.length;
            quantityInBuffer--;
            System.out.printf("Consuming value: %s", consumedValue);
            canWrite.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }

        return consumedValue;
    }
}
