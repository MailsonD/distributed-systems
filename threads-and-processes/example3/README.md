# Utilizando sincronização em um Buffer circular

No exemplo anterior resolvemos o problema da sincronização, porém devido as Threads passarem bastante tempo ociosas esperando pelo recurso nos a um problema de lentidão. Uma forma de contornar isso é implementando um buffer circula, o qual disponibiliza mais espaços para serem preenchidos pelo produtor, fazendo assim com que ele não precise esperar sempre pelo consumidor e vice versa.

## Solução

Para implementarmos o buffer circular iremos trabalhar com um array de valores dentro do buffer, que nos dará mais espaço para armazenar os dados produzidos, fazendo assim com que o consumo e a produção possam ter uma margem de atraso no seu tempo de execução sem afetar o desempenho de todas as Threads.

Teremos também duas variáveis indicando em qual índice do array ocorrerá a próxima escrita a próxima leitura. Essas alterações foram feitas apenas no arquivo de implementação do Buffer, o PoemBuffer.
