# Processos e Threds - Mailson Dennis

Neste repositório encontraremos 4 exemplos trabalhando o conceito de processos e threads. Os exemplos foram feitos em um mesmo domínio apenas com intuito de ir evoluindo suas implementaçõs a cada novo exemplo, o que resultou em vários códigos iguais em todos os exemplos.

O domínio escolhido foi o de utilizar Threads (uma para produzir e outra para consumir) com o intuito de recitar os versos de um poema em ordem.

## Like the Moon

A baixo se encontram os versos do poema escolhido e que devem ser recitados na ordem em que se encontram aqui.

I will be <br/>
like the moon <br/>
and learn to shine <br/>
even when <br/>
I am not whole. <br/>

## Exemplos

Os exemplos podem ser acessados a partir dos links abaixo.

- <a href="./example1/README.md" >Exemplo 1</a>
- <a href="./example2/README.md" >Exemplo 2</a>
- <a href="./example3/README.md" >Exemplo 3</a>
- <a href="./example4/README.md" >Exemplo 4</a>

## Como Rodar

Os exemplos foram construídos utilizando o maven, então é recomendao a utilização do maven.

Para compilar o código do projeto a única coisa necessária é o comando a seguir dentro da pasta do projeto

```
mvn compile
```

E para rodar cada um dos exemplos, após compilar o código, rode os comandos

```
cd target/classes
```

```
java App
```
