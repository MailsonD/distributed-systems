package flow;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class PoemBuffer implements Buffer{

    private final Lock accessLock = new ReentrantLock();
    private final Condition canWrite = accessLock.newCondition();
    private final Condition canRead = accessLock.newCondition();

    private String buffer;
    private boolean hasNewValue;

    public PoemBuffer() {
        this.buffer = "";
        this.hasNewValue = false;
    }

    @Override
    public void set(String value) {
        accessLock.lock();
        try {
            while(hasNewValue) {
                System.out.println("Trying to write");
                System.out.println("Buffer in use. The producer awaits!");
                canWrite.await();
            }
            buffer = value;
            hasNewValue = true;
            System.out.printf("Producing value: %s", value);
            canRead.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }

    }

    @Override
    public String get() {
        accessLock.lock();
        try {
            while(!hasNewValue) {
                System.out.println("Trying to read");
                System.out.println("Buffer empty. The consumer awaits!");
                canRead.await();
            }
            hasNewValue = false;
            System.out.printf("Consuming value: %s", buffer);
            canWrite.signal();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            accessLock.unlock();
        }

        return buffer;
    }
}
