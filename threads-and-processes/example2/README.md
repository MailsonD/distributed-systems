# Utilizando sincronização em multithreads

Neste exemplo iremos dar continuidade ao exemplo anterior, porém agora resolvend o problem de sincronização entre as Threads. Devido a aleatoriedade de tempo entre a execução das Threads de consumo e de produção, podemos notar que saída pode não vir na ordem em que desejamos. Para isso, precisamos sincronizar as Threads para que o consumo só seja feito após a produção, e a próxima produção apenas após o ultimo consumo.

## Utilizando o Lock para sincronia

Java dispõe da utilização de <i>Locks</i>, os quais podemos utilizar para sincronizar o uso de um recurso por multiplas threads.

Uma vez que o recurso estiver em posse de uma Thread podemos então criar essa trava que impede que outra Thread utilize desse mesmo recurso enquanto a primeira Thread não finalizar a sua execução com aquele recurso.

Iremos então usar a trava no recurso do buffer através do métod `lock()`, lembrando sempre de desbloquear o recurso através do método `unlock()`.

O lock, por sua vez, disponibiliza condicionais que podemos utlizar quando queremos fazer uma Thread esperar por um certo tempo para que o recruso necessário venha a ficar disponível. Também fazemos uso dessas condicionais no exemplo para a sincronização.

OBS.
A única classe alterada nesse exemplo e nos próximos é a implementação do Buffer.
